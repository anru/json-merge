# README

## Building

Project can be built with support for one of the two included `JSON` libraries.
Target `json-main-with-jansson` builds the application and includes `jannson`
library. Target `json-main-with-cjson` builds the application with `cJSON`
library. If you wish to build another variant of application after having built
one, be sure to execute `make clean`.

Example:

```bash
make json-main-with-jansson
```

```bash
make json-main-with-cjson
```

## Running

Running the command below would output the merged result for files
`examples/sample1.json` and `examples/sample2.json`. Output file can be
specified with option `-o`; `stdout` is used if this option is absent.

```bash
./json-main-with-jansson examples/sample1.json examples/sample2.json
```
Errors are printed to `stderr` so a partial merge without erroneous files can be
obtained with command below:

```bash
./json-main-with-jansson examples/sample1.json examples/sample2.json 2>/dev/null
```

One file can be specified multiple times. File contents and parsed objects are
kept in memory simultaneously, this can lead to memory issues when a large
number of files are merged.

## Merge strategies

Four merge strategies are supported:

+ `add_missing` (and do not overwrite existing)
+ `overwrite_existing` (and add missing)
+ `recursive`
+ `rename` (duplicates)

`add_missing` would start out with the first file and add only those keys from
subsequent files that are not already present.

`overwrite_existing` would add keys from subsequent files, but, upon encountering
a key that already exists, replace its value. This mode is closest to RFC7396
which is supported by `cJSON`.

`recursive` merges matching label's content.

`rename` adds a random suffix when encountering an already present key.

## Pretty printing

Application does not output a "pretty printed" JSON. If necessary, this can be
achieved with Python:

```bash
./json-main-with-jansson -s add_missing examples/modem.status.fixed examples/radiod.conf | python3 -m json.tool -
```

## TODO

+ Array merging does not work at the moment.

## Libraries

A number of libraries are compared in a benchmark project here
`https://github.com/miloyip/nativejson-benchmark`. Only C libraries are
considered per requirements. Additionally the library should support error
reporting and serializing the parsed object.
Out of those `cJSON` (prior experience) and `jansson` were chosen. These are
actively maintained and have clean comprehensible interfaces.

### cJSON

`cJSON` library is available at `https://github.com/DaveGamble/cJSON`. It has
some drawbacks in scope of the exercise: error reporting is rudimentary, without
line number in source file and only one merge strategy is supported.
Functions `cJSONUtils_MergeMissing`, `cJSONUtils_MergeRecursive`,
`cJSONUtils_MergeRename` are added.

### jansson

`jansson` is available at `https://github.com/akheron/jansson/`. Parse error
includes line number in source file and several merge strategies (functions
`json_object_update_*`).
Functions `json_object_update_merge`, `json_object_update_rename` are added.

There are a number of other libraries that could be used. Those most promising
are listed below.

+ json-c: `https://github.com/json-c/json-c`
+ parson: `https://github.com/kgabis/parson`
+ yajl: `https://github.com/lloyd/yajl`
+ json-parser:
`https://github.com/udp/json-parser`
`https://github.com/udp/json-builder`
