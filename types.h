#ifndef _TYPES_H
#define _TYPES_H 1

#include <stdlib.h>

#if defined(USE_CJSON)
#include "cJSON.h"
#elif defined(USE_JANNSON)
#include "jansson.h"
#endif /* USE_CJSON, USE_JANNSON */

typedef struct {
    char *file_name;
    char *contents;
    size_t size;
    /* JSON struct type could be defined depending on used library
     * and the field `root` defined without IFDEFs
     * but that seems to be more error prone.
     */
#if defined(USE_CJSON)
    cJSON *root;
#elif defined(USE_JANNSON)
    json_t *root;
#else
    void *root;
#endif /* USE_CJSON, USE_JANNSON */
    // Optionally an error field could be added
} file_contents_t;

#define ADD_MISSING_STR "add_missing"
#define OVERWRITE_EXISTING_STR "overwrite_existing"
#define RECURSIVE_STR "recursive"
#define RENAME_DUPLICATES_STR "rename"

typedef enum {
    ADD_MISSING,
    OVERWRITE_EXISTING,
    RECURSIVE,
    RENAME_DUPLICATES
} merge_strategy_t;


#endif /* _TYPES_H */