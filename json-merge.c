#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

#include "json-merge.h"
#include "utils.h"

#ifdef USE_CJSON
#include "cJSON.h"
#include "cJSON_Utils.h"
#elif defined(USE_JANNSON)
#include "jansson.h"
#endif /* USE_CJSON, USE_JANNSON */

/*
 * Join passed source objects together in a JSON object using provided
 * merge strategy.
 *
 * @param count - number of sources
 * @param sources - source objects
 * @param destination - container for resulting JSON object
 * @param merge_strategy - merge strategy
 * @return 0 in case of success, 1 in case of failure
 */
int json_merge_contents(int files_count, file_contents_t *const sources,
        file_contents_t *const destination, merge_strategy_t merge_strategy)
{
    int rc = 0;
#ifdef USE_CJSON
    destination->root = cJSON_CreateObject();
#elif defined USE_JANNSON
    destination->root = json_object();
#endif /* USE_CJSON, USE_JANNSON */

    for (int i = 0; i < files_count; i++) {
        if (sources[i].root != NULL) {
            int rc_merge = 0;
#ifdef USE_CJSON
            switch (merge_strategy) {
                case ADD_MISSING:
                    rc_merge = cJSONUtils_MergeMissing(destination->root,
                        sources[i].root) == NULL ? 1 : 0;
                    break;
                case OVERWRITE_EXISTING:
                    rc_merge = cJSONUtils_MergePatch(destination->root,
                        sources[i].root) == NULL ? 1 : 0;
                    break;
                case RECURSIVE:
                    rc_merge = cJSONUtils_MergeRecursive(destination->root,
                        sources[i].root) == NULL ? 1 : 0;
                    break;
                case RENAME_DUPLICATES:
                    rc_merge = cJSONUtils_MergeRename(destination->root,
                        sources[i].root) == NULL ? 1 : 0;
                    break;
                default:
                    fprintf(stderr, "Unsupported merge strategy\n");
                    rc = 1;
                    goto break_loop;
            }
#elif defined USE_JANNSON
            switch (merge_strategy) {
                case ADD_MISSING:
                    rc_merge = json_object_update_missing(destination->root,
                        sources[i].root);
                    break;
                case OVERWRITE_EXISTING:
                    rc_merge = json_object_update_merge(destination->root,
                        sources[i].root);
                    break;
                case RECURSIVE:
                    rc_merge = json_object_update_recursive(destination->root,
                        sources[i].root);
                    break;
                case RENAME_DUPLICATES:
                    rc_merge = json_object_update_rename(destination->root,
                        sources[i].root);
                    break;
                default:
                    fprintf(stderr, "Unsupported merge strategy\n");
                    rc = 1;
                    goto break_loop;
            }

            if (rc_merge == 0) {
                sources[i].root = NULL;
            }
#endif /* USE_CJSON, USE_JANNSON */
            rc = rc_merge != 0 ? 1 : 0;
        }
    }
break_loop:
    return rc;
}

/*
 * Read and merge JSON files and optionally write the result in provided
 * destination file using specified merge strategy.
 *
 * @param files_count - number of sources
 * @param files - source file names
 * @param dest - destination file name
 * @param merge_strategy - merge strategy
 * @return 0 in case of success, 1 in case of failure
 */
int json_merge(unsigned int files_count, char **files, char *dest,
        merge_strategy_t merge_strategy)
{
    if (files_count == 0 || files == NULL) {
        // nothing to merge
        fprintf(stderr, "No files to merge\n");
        return 1;
    }

    int rc = 0;

    file_contents_t *sources = NULL;
    file_contents_t destination;

    destination.file_name = dest;
    destination.contents = NULL;
#if defined(USE_JANNSON)
    destination.root = NULL;
#elif defined(USE_CJSON)
    destination.root = NULL;
#endif /* USE_JANNSON, USE_CJSON */

    sources = (file_contents_t *)calloc(files_count, sizeof(file_contents_t));
    if (sources == NULL) {
        fprintf(stderr, "Could not allocate memory for service structs\n");
        return 1; // TODO ENOMEM
    }

    for (int i = 0; i < files_count; i++) {
        sources[i].file_name = files[i];
    }

    for (int i = 0; i < files_count; i++) {
        sources[i].contents = NULL;
        read_file(&sources[i]);
        if (sources[i].contents == NULL) {
            fprintf(stderr, "File %s not loaded\n", sources[i].file_name);
            rc = 1;
        }
        else {
#ifdef USE_CJSON
            sources[i].root = cJSON_Parse(sources[i].contents);
            const char *parse_error = cJSON_GetErrorPtr();
            if (parse_error != NULL) {
                if (sources[i].root != NULL) {
                    cJSON_Delete(sources[i].root);
                    sources[i].root = NULL;
                }
                fprintf(stderr, "Error parsing file %s: %s\n",
                    sources[i].file_name, parse_error);
                rc = 1;
            }
#elif defined(USE_JANNSON)
            json_error_t error;
            sources[i].root = json_loads(sources[i].contents,
                JSON_DISABLE_EOF_CHECK, &error);
            if (sources[i].root == NULL) {
                fprintf(stderr, "Error parsing file %s on line %d: %s\n",
                    sources[i].file_name, error.line, error.text);
                rc = 1;
            }
#endif /* USE_CJSON, USE_JANNSON */
        }
    }

    if (sources != NULL) {
        int rc_merge = json_merge_contents(files_count, sources, &destination,
            merge_strategy);
        rc = (rc == 0) ? rc_merge : rc;
    }


    if (destination.root != NULL) {
#ifdef USE_CJSON
        destination.contents = cJSON_PrintUnformatted(destination.root);
#elif defined(USE_JANNSON)
        destination.contents = json_dumps(destination.root, 0);
#endif /* USE_CJSON, USE_JANNSON */
        if (destination.contents != NULL) {
            // TODO strnlen would be better
            destination.size = strlen(destination.contents);
        }
        if (destination.file_name == NULL) {
            /* If output file not provided, print the result on stdout */
            fprintf(stdout, "%s\n", destination.contents);
        }
        else {
            write_file(&destination);
        }
    }

    if (sources != NULL) {
        for (int i = 0; i < files_count; i++) {
            if (sources[i].contents != NULL) {
                free(sources[i].contents);
            }
            if (sources[i].root != NULL) {
#ifdef USE_CJSON
                cJSON_Delete(sources[i].root);
#elif defined(USE_JANNSON)
                json_decref(sources[i].root);
#endif /* USE_CJSON, USE_JANNSON */
            }
        }
        free(sources);
    }

    if (destination.root != NULL) {
#ifdef USE_CJSON
        cJSON_Delete(destination.root);
#elif defined(USE_JANNSON)
        json_decref(destination.root);
#endif /* USE_CJSON, USE_JANNSON */
    }

    if (destination.contents != NULL) {
        free(destination.contents);
    }

    return rc;
}

