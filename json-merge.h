#ifndef _JSON_MERGE_H
#define _JSON_MERGE_H 1

#include "types.h"

int json_merge(unsigned int files_count, char **files, char *destination,
    merge_strategy_t merge_strategy);

#endif /* _JSON_MERGE_H */