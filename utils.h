#ifndef _UTILS_H
#define _UTILS_H 1

#include "types.h"

char* read_file(file_contents_t *const fcontents);
int write_file(const file_contents_t *const fcontents);

#endif /* _UTILS_H */
