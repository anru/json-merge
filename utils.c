#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>

#include "utils.h"

#if 0
int read_file_g(const char *const filename)
{
    int rc = 0;

    FILE *fp;
    struct stat filestatus;
    int file_size;
    char* file_contents;

    if (stat(filename, &filestatus) != 0) {
        fprintf(stderr, "File %s not found\n", filename);
        return 1;
    }

    file_size = filestatus.st_size;
    file_contents = (char*)malloc(filestatus.st_size);
    if (file_contents == NULL) {
        fprintf(stderr, "Memory error: unable to allocate %d bytes\n", file_size);
        rc = 1;
        goto exit;
    }

    fp = fopen(filename, "rt");
    if (fp == NULL) {
        fprintf(stderr, "Unable to open %s\n", filename);
        rc = 1;
        goto exit_free_resources;
    }

    if (fread(file_contents, file_size, 1, fp) != 1) {
        fprintf(stderr, "Unable t read content of %s\n", filename);
        rc = 1;
        goto exit_free_resources;
    }

exit_free_resources:
    fclose(fp);
    free(file_contents);

exit:
    return rc;
}
#endif

/*
 * Returns a pointer to file contents in memory or NULL on failure.
 * It is up to the caller to free the memory.
 */
char* read_file(file_contents_t *const fcontents)
{
    FILE *fp;
    struct stat filestatus;
    size_t file_size;
    char* file_contents;

    if (stat(fcontents->file_name, &filestatus) != 0) {
        fprintf(stderr, "File %s not found\n", fcontents->file_name);
        return NULL;
    }

    file_size = filestatus.st_size;
    file_contents = (char *)malloc(sizeof(char) * (file_size + 1));
    if (file_contents == NULL) {
        fprintf(stderr, "Memory error: unable to allocate %lu bytes\n",
            file_size + 1);
        return NULL;
    }

    memset(file_contents, 0, file_size + 1);

    fp = fopen(fcontents->file_name, "r");
    if (fp == NULL) {
        fprintf(stderr, "Unable to open %s\n", fcontents->file_name);
        free(file_contents);
        file_contents = NULL;
        file_size = 0;
        return NULL;
    }

    size_t size_read = fread(file_contents, 1, file_size, fp);
    if (size_read == 0 || ferror(fp)) {
        fprintf(stderr, "Unable to read content of %s\n", fcontents->file_name);
        fclose(fp);
        free(file_contents);
        file_contents = NULL;
        file_size = 0;
        return NULL;
    }
    fclose(fp);

    if (size_read != file_size) {
        fprintf(stderr, "Read only %lu bytes out of %lu\n", size_read, file_size);
    }

    file_contents[size_read] = 0;

    fcontents->size = size_read;
    fcontents->contents = file_contents;

    return file_contents;
}


int write_file(const file_contents_t *const fcontents)
{
    FILE *fp;
    fp = fopen(fcontents->file_name, "w");
    if (fp == NULL) {
        fprintf(stderr, "Unable to open %s for writing\n", fcontents->file_name);
        fclose(fp);
        return 1;
    }
    fwrite(fcontents->contents, sizeof(char), fcontents->size, fp);
    fclose(fp);
    return 0;
}
