# Following http://nuclear.mutantstargoat.com/articles/make/

# CC=clang
CC=gcc

CFLAGS=-std=c99 -Wall
json-main-with-cjson: CFLAGS+=-Icjson -DUSE_CJSON -DENABLE_CJSON_UTILS=On
json-main-with-jansson: CFLAGS+=-Ijansson -DHAVE_CONFIG_H -DUSE_JANNSON

LDFLAGS=-g

src_cjson = $(wildcard *.c) $(wildcard cjson/*.c)
obj_cjson = $(src_cjson:.c=.o)

src_jansson = $(wildcard *.c) $(wildcard jansson/*.c)
obj_jansson = $(src_jansson:.c=.o)

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

# By default with the jannson library
all: json-main-with-jansson

# all: json-main-with-cjson

json-main-with-jansson: $(obj_jansson)
	$(CC) -o $@ $^ $(LDFLAGS)

json-main-with-cjson: $(obj_cjson)
	$(CC) -o $@ $^ $(LDFLAGS)

.PHONY: clean
clean:
	rm -f $(obj_cjson) $(obj_jansson) json-main-with-cjson json-main-with-jansson
