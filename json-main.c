#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <getopt.h>
#include <ctype.h>

#include "json-merge.h"

#define VERSION "v0.0.1"
#define SPACER "                           "

#define no_argument 0
#define required_argument 1
#define optional_argument 2

const struct option longopts[] =
{
    {"output", required_argument, 0, 'o'},
    {"strategy", required_argument, 0, 's'},
    {"version", no_argument, 0, 'v'},
    {0, 0, 0, 0},
};

/*
 * Print commandline application usage information
 */
void usage(char* executable)
{
    fprintf(stderr, "Usage: %s [options] <json file>...\n", executable);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "  -o <file>, --output=<file>\n"
        SPACER
        "Write the output into <file>.\n");
    fprintf(stderr, "  -s, --strategy           Merge strategy. Can be one of "
        "the following values:\n"
        SPACER
        " add_missing, overwrite_existing, recursive, rename\n"
    );
    fprintf(stderr, "  -v, --version            Print version information.\n");
    fprintf(stderr, "\n");
}

int main(int argc, char** argv)
{
    if (argc < 2) {
        usage(argv[0]);
        return 1;
    }

    int rc = 0;

    int index;
    int iarg = 0;

    int files_count = 0;
    char **files = NULL;
    char *destination_file = NULL;
    merge_strategy_t merge_strategy = ADD_MISSING;

    // turn off getopt error message
    opterr = 0;

    while (iarg != -1) {
        iarg = getopt_long(argc, argv, "o:s:v", longopts, &index);
        if (iarg == 'o') {
            destination_file = optarg;
        }
        else if (iarg == 's') {
            if (strncmp(ADD_MISSING_STR, optarg, sizeof(ADD_MISSING_STR)) == 0) {
                merge_strategy = ADD_MISSING;
            }
            else if (strncmp(OVERWRITE_EXISTING_STR, optarg, sizeof(OVERWRITE_EXISTING_STR)) == 0) {
                merge_strategy = OVERWRITE_EXISTING;
            }
            else if (strncmp(RECURSIVE_STR, optarg, sizeof(RECURSIVE_STR)) == 0) {
                merge_strategy = RECURSIVE;
            }
            else if (strncmp(RENAME_DUPLICATES_STR, optarg, sizeof(RENAME_DUPLICATES_STR)) == 0) {
                merge_strategy = RENAME_DUPLICATES;
            }
            else {
                fprintf(stderr, "Unknown merge strategy: %s\n", optarg);
                usage(argv[0]);
                return 1;
            }
        }
        else if (iarg == 'v') {
            fprintf(stdout, "Version: %s\n", VERSION);
            return 0;
        }
        else if (iarg == '?')  {
            if (isprint(optopt)) {
                fprintf(stdout, "Unknown option: %c\n", optopt);
            }
            else {
                fprintf(stdout, "Unknown option: error-0x%08x\n", optopt);
            }
            usage(argv[0]);
            return 1;
        }
    }

    files_count = argc - optind;
    if (files_count > 0) {
        files = (char **)calloc(files_count, sizeof(char *));
        if (files == NULL) {
            // not enough memory
            fprintf(stderr, "Could not allocate memory for files list\n");
            rc = 1;
        }
        else {
            for (int i = optind, j = 0; i < argc; i++, j++) {
                files[j] = argv[i];
            }
        }
    }
    else {
        // nothing to merge
        usage(argv[0]);
        rc = 1;
    }

    if (rc == 0) {
        rc = json_merge(files_count, files, destination_file, merge_strategy);
    }

    if (files != NULL) {
        free(files);
    }

    return rc;
}